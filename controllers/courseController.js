const Course = require("../models/Course");
const User = require('../models/User');
const auth = require("../auth");


// Creating a new course


module.exports.addCourse = (reqBody) => {
	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	});

	return newCourse.save().then((course, error) => {
		if (error) {
			return false
		}else{
			return true
		}
	})
};

// module.exports.checkUserisAdmin = (courseData, reqbody) => {
// 	return User.findById(courseData.id).then(result => {
			 
// 			if (result.isAdmin != true) {
// 				return false
// 			}else{
				
// 				return true

					// let newCourse = new Course({
					// 		name: courseData.name,
					// 		description: courseData.description,
					// 		price: courseData.price
					// 	});
					
					// return newCourse.save().then((course, error) =>{
					// 	if (error) {
					// 		return false
					// 	}else{
					// 		return true
					// 	}
					// })
				


			// }

		


// 	})

// };

				
// Retrieving for all courses
module.exports.getAllCourses = (data) => {

	if (data.isAdmin) {

		return Course.find({}).then(result => {
			return result
		})
	}else{
		return false
	}
}


// Retrieve all active courses
module.exports.getAllActive = () =>{

	return Course.find({isActive: true}).then(result =>{
		return result
	})
}





// Retrieve a specific course
module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result =>{
		return result
	})
};







module.exports.updateCourse = (reqParams, reqBody) => {
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}
	
	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((updatedCourse, error) => {
		if (error) {
			return false
		}else{
			return true
		}
	})
}



// Activity 40
module.exports.updateCourseIsActive = (reqParams, reqBody) => {
	let updatedCourse = {
		
		isActive: reqBody.isActive
	}
	
	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((updatedCourse, error) => {
		if (error) {
			return false
		}else{
			return true
		}
	})
}
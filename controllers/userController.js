const User = require('../models/User');
const bcrypt = require("bcrypt");
const auth = require("../auth");


// 
// Controller Functions

// Checking Email
module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {
		console.log(result.email)
		if(result.length > 0) {
			return true
		} else {
			return false
		}
	})
};


// User Registration
module.exports.registerUser = (reqBody) => {
	
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, error) => {
		// User registration failed
		if(error) {
			return false
		// User registration successful
		} else {
			return true
		}
	})
};


// User Log in
module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null) {
			return false
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result)}
			} else {
				return false
			}
		}
	})
};

module.exports.getProfile = (data) => {
	return User.findById(data.id).then(result => {
		console.log(data.id);
		console.log(result);
		// result.password = "*****"
		if (result == null) {
			return false
		}else{
			result.password = "*****"
		}
		// Returns the user information with the password as an empty string or asterisk.
		return result
	})
};